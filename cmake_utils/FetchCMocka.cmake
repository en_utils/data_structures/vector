function(get_cmocka)
    FetchContent_Declare(
            cmocka
            GIT_REPOSITORY https://github.com/clibs/cmocka.git
            GIT_TAG cmocka-1.1.5
    )

    set(WITH_STATIC_LIB ON CACHE BOOL "CMocka: Build with a static library" FORCE)
    set(WITH_CMOCKERY_SUPPORT ON CACHE BOOL "CMocka: Install a cmockery header" FORCE)
    set(WITH_EXAMPLES OFF CACHE BOOL "CMocka: Build examples" FORCE)
    set(UNIT_TESTING ON CACHE BOOL "CMocka: build with unit testing" FORCE)
    set(PICKY_DEVELOPER ON CACHE BOOL "CMocka: Build with picky developer" FORCE)
    FetchContent_MakeAvailable(cmocka)
endfunction()