//
// Created by everythingnice on 3/7/24.
// Code Coverage: 87%
//


#include <m_vec.h>
#include <m_vec_iter.h>

// It seems cmocka is annoying and requires these to be imported in this order.
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <setjmp.h>
#include <cmocka.h>



VECTOR_CREATE_HEADER(int_vec_m, int_vec, int)
VECTOR_CREATE_SOURCE(int_vec_m, int_vec, int)

/**
 * @brief Initializes the vector tests.
 *
 * This function is used to test the initialization of a vector.
 * It initializes a vector, checks the status, verifies the pointer to the vector,
 * buffer, capacity, and size. It then clears the vector and checks the status again.
 * Finally, it tests the initialization with a NULL pointer and checks the status.
 *
 * @param state A pointer to the state of the test case (unused).
 */
static void init_vector_tests(void **state) {
    (void) state;
    bool status;
    int_vec_m vec;
    status = int_vec_init(&vec);

    assert_true(status);
    assert_non_null(&vec);
    assert_non_null(vec.buffer);
    assert_int_equal(vec.capacity, INITIAL_CAPACITY);
    assert_int_equal(vec.size, 0);
    int_vec_clear(&vec);

    status = int_vec_init(NULL);
    assert_false(status);
}

/**
 * @brief Function to create unit tests for the create_vector_tests function.
 *
 * This function is used to create unit tests for the create_vector_tests function.
 * It checks whether the create_vector_tests function creates a vector correctly,
 * with the correct initial capacity and size.
 *
 * @param[in,out] state A pointer to the test state
 */
static void create_vector_tests(void **state) {
    (void) state;
    int_vec_m *vec = int_vec_create();
    assert_non_null(vec);
    assert_non_null(vec->buffer);
    assert_int_equal(vec->capacity, INITIAL_CAPACITY);
    assert_int_equal(vec->size, 0);

    int_vec_destory(vec);
}

/**
 * @brief Test cases for the function int_vec_reserve.
 *
 * This function is used to test the int_vec_reserve function. It performs
 * several test cases to validate the behavior of int_vec_reserve.
 *
 * @param state A pointer to the test state object.
 */
static void reserve_vector_tests(void **state) {
    (void) state;
    int_vec_m vec;
    bool status;
    status = int_vec_init(&vec);
    assert_true(status);
    assert_non_null(&vec);
    assert_non_null(vec.buffer);
    assert_int_equal(vec.capacity, INITIAL_CAPACITY);
    assert_int_equal(vec.size, 0);

    // Reserve a large amount of space should succeed
    status = int_vec_reserve(&vec, INT16_MAX);
    assert_true(status);
    assert_int_equal(vec.capacity, INT16_MAX);

    // Reserve a smaller amount of space than is already reserved, should fail
    status = int_vec_reserve(&vec, INT8_MAX);
    assert_false(status);

    int_vec_clear(&vec);

    // Reserve space given a buffer that points to null. Should succeed.
    assert_int_equal(vec.capacity, 0);
    status = int_vec_reserve(&vec, INT8_MAX);
    assert_true(status);
    assert_int_equal(vec.capacity, INT8_MAX);

    int_vec_clear(&vec);
}

/**
 * @brief Unit tests for the `push_back` function of the `int_vec_m` type.
 *
 * This function contains unit tests to verify the behavior of the `push_back`
 * function of the `int_vec_m` type. It tests various scenarios including vector
 * creation, adding a single value, and vector resizing.
 *
 * @param state The testing state.
 */
static void push_back_tests(void **state) {
    (void) state;
    int_vec_m vec;
    bool status;
    status = int_vec_init(&vec);

    // Ensure the vector is created correctly
    assert_true(status);
    assert_non_null(&vec.buffer);
    assert_int_equal(vec.size, 0);
    assert_int_equal(vec.capacity, INITIAL_CAPACITY);

    // Check if we can add one value
    status = int_vec_push_back(&vec, 1);
    assert_true(status);
    assert_int_equal(vec.size, 1);
    assert_int_equal(vec.buffer[0], 1);
    assert_int_equal(vec.capacity, INITIAL_CAPACITY);
    int_vec_clear(&vec);

    // Check if the vector resizes correctly
    int_vec_reserve(&vec, INITIAL_CAPACITY);
    for (size_t i = 0; i < INITIAL_CAPACITY + 1; i++) {
        status = int_vec_push_back(&vec, (int) i);
        assert_true(status);
        assert_int_equal(vec.buffer[i], i);
    }

    assert_int_equal(vec.capacity, INITIAL_CAPACITY * GROWTH_MODIFIER);
    int_vec_clear(&vec);
}

/**
 * @brief Test cases for the `pop_back` function in the `int_vec` module.
 *
 * @param state The current state of the test runner.
 */
static void pop_back_tests(void **state) {
    (void) state;
    int_vec_m vec;
    bool status;
    int ret_val;
    status = int_vec_init(&vec);

    assert_true(status);
    assert_non_null(&vec.buffer);
    assert_int_equal(vec.size, 0);
    assert_int_equal(vec.capacity, INITIAL_CAPACITY);

    // Test that we can pop at least one value
    status = int_vec_push_back(&vec, 1);
    assert_true(status);

    ret_val = int_vec_pop_back(&vec);
    assert_int_equal(vec.size, 0);
    assert_int_equal(vec.capacity, INITIAL_CAPACITY);
    assert_int_equal(ret_val, 1);

    int_vec_clear(&vec);
    int_vec_reserve(&vec, INITIAL_CAPACITY);

    // Test popping multiple values
    for (int i = 0; i < INITIAL_CAPACITY + 1; i++) {
        status = int_vec_push_back(&vec, i);
        assert_true(status);
    }

    for (int j = INITIAL_CAPACITY; j >= 0; j--) {
        ret_val = int_vec_pop_back(&vec);
        assert_int_equal(ret_val, j);
    }

    assert_int_equal(vec.size, 0);
    assert_int_equal(vec.capacity, INITIAL_CAPACITY * GROWTH_MODIFIER);

    int_vec_clear(&vec);
}

/**
 * @brief Executes the unit tests for the `int_vec_m` data structure.
 *
 * This function initializes an `int_vec_m` object, pushes `INITIAL_CAPACITY` integers
 * onto it, and then verifies that each integer can be accessed using `int_vec_at`.
 * Finally, it clears the `int_vec_m` to free the allocated memory.
 *
 * @param state The state for the unit test.
 *
 * @remarks The `int_vec_m` type and related functions are not documented here, as their
 * definitions are not provided.
 */
static void at_tests(void **state) {
    (void) state;
    int_vec_m vec;
    bool status;
    status = int_vec_init(&vec);
    assert_true(status);

    for (int i = 0; i < INITIAL_CAPACITY; i++) {
        status = int_vec_push_back(&vec, i);
        assert_true(status);
    }

    for (int j = 0; j < INITIAL_CAPACITY; j++) {
        int val = int_vec_at(&vec, j);
        assert_int_equal(val, j);
    }

    int_vec_clear(&vec);
}

/**
 * @brief Function to perform back tests on the int_vec_m vector.
 *
 * @param state The state of the test.
 */
static void back_tests(void **state) {
    (void) state;
    int_vec_m vec;
    bool status;
    const int final = 150;
    status          = int_vec_init(&vec);
    assert_true(status);

    status = int_vec_push_back(&vec, 1);
    assert_true(status);
    assert_int_equal(int_vec_back(&vec), 1);

    int_vec_clear(&vec);
    int_vec_reserve(&vec, INITIAL_CAPACITY);

    for (int i = 0; i < final; i++) {
        status = int_vec_push_back(&vec, i);
        assert_true(status);
    }

    assert_int_equal(int_vec_back(&vec), final - 1);
    int_vec_clear(&vec);
}

/**
 * @brief Perform tests for the data operations of int_vec_m.
 *
 * @param state The state parameter for the test case.
 */
static void data_tests(void **state) {
    (void) state;
    int_vec_m vec;
    bool status;
    status = int_vec_init(&vec);
    assert_true(status);

    int *data = int_vec_data(&vec);
    assert_non_null(data);

    data = int_vec_data(NULL);
    assert_null(data);

    int_vec_clear(&vec);
}

/**
 * @brief Function to perform capacity tests on int_vec_m structure.
 *
 * This function tests the capacity-related functions of int_vec_m structure.
 * It initializes an int_vec_m structure, checks the initial capacity,
 * reserves memory to double the capacity, clears the vector, and checks the capacity again.
 * It also tests the behavior of passing a NULL pointer to int_vec_capacity() function.
 *
 * @param state A pointer to the test state
 */
static void capacity_tests(void **state) {
    (void) state;
    int_vec_m vec;
    bool status;
    status = int_vec_init(&vec);
    assert_true(status);

    assert_int_equal(int_vec_capacity(&vec), INITIAL_CAPACITY);

    int_vec_reserve(&vec, (size_t) INITIAL_CAPACITY * GROWTH_MODIFIER);
    assert_int_equal(int_vec_capacity(&vec), INITIAL_CAPACITY * 2);

    int_vec_clear(&vec);
    assert_int_equal(int_vec_capacity(&vec), 0);

    assert_int_equal(int_vec_capacity(NULL), OPERATION_ERROR);
}

/**
 * @brief Perform size tests on the int_vec_m structure.
 *
 * This function performs various tests to validate the behavior of the `int_vec_m` structure with regards to its size.
 *
 * @param state A pointer to the test state (not used in this function).
 */
static void size_tests(void **state) {
    (void) state;
    int_vec_m vec;
    bool status;
    status = int_vec_init(&vec);
    assert_true(status);

    assert_int_equal(int_vec_size(&vec), 0);

    for (int i = 0; i < INITIAL_CAPACITY; i++) {
        status = int_vec_push_back(&vec, i);
        assert_true(status);
    }

    assert_int_equal(int_vec_size(&vec), INITIAL_CAPACITY);

    int_vec_clear(&vec);
    assert_int_equal(int_vec_size(&vec), 0);

    assert_int_equal(int_vec_size(NULL), OPERATION_ERROR);
}

/**
 * @brief This function tests the functionality of int_vec_empty, int_vec_push_back, and int_vec_clear.
 *
 * @param state The state parameter for the test.
 */
static void empty_tests(void **state) {
    (void) state;
    bool status;
    int_vec_m vec;
    status = int_vec_init(&vec);
    assert_true(status);

    assert_true(int_vec_empty(&vec));
    status = int_vec_push_back(&vec, 1);
    assert_true(status);
    assert_false(int_vec_empty(&vec));

    int_vec_clear(&vec);
}

/**
 * @brief Perform front tests on the int_vec_m structure.
 * @param state Double pointer to the state.
 * @details This function initializes an int_vec_m structure, checks if the initialization is successful,
 *          retrieves the front element using int_vec_front, verifies that the front element is not NULL,
 *          and finally clears the int_vec_m structure.
 */
static void front_tests(void **state) {
    (void) state;
    int_vec_m vec;
    bool status;
    status = int_vec_init(&vec);
    assert_true(status);

    int *front = int_vec_front(&vec);
    assert_non_null(front);

    int_vec_clear(&vec);
}

/**
 * @brief Insert tests for the int_vec_m data structure.
 *
 * This function performs various tests for the int_vec_m data structure. It initializes a new int_vec_m object,
 * inserts elements into it, and checks if the insertions were successful.
 */
static void insert_tests(void **state) {
    (void) state;
    bool status;
    int_vec_m vec;
    status = int_vec_init(&vec);
    assert_true(status);

    for (int i = 0; i < UINT8_MAX; i++) {
        status = int_vec_insert(&vec, i, i);
        assert_true(status);
        assert_int_equal(vec.buffer[i], i);
    }

    int_vec_clear(&vec);
    status = int_vec_reserve(&vec, INITIAL_CAPACITY);
    assert_true(status);

    for (int i = 0; i < INITIAL_CAPACITY; i++) {
        status = int_vec_push_back(&vec, i);
        assert_true(status);
    }

    status = int_vec_insert(&vec, 2, UINT8_MAX);
    assert_true(status);
    assert_int_equal(vec.buffer[2], UINT8_MAX);

    int_vec_clear(&vec);
}

/**
 * @brief This function tests the functionality of resizing an int_vec_m structure.
 *
 * @param state The state of the test case.
 */
static void resize_tests(void **state) {
    (void) state;
    int_vec_m vec;
    bool status;
    status = int_vec_init(&vec);
    assert_true(status);

    status = int_vec_resize(&vec, INITIAL_CAPACITY + 1);
    assert_true(status);
    assert_int_equal(vec.size, 0);
    assert_int_equal(vec.capacity, INITIAL_CAPACITY + 1);

    status = int_vec_resize(&vec, INITIAL_CAPACITY + 1);
    assert_true(status);
    assert_int_equal(vec.size, 0);
    assert_int_equal(vec.capacity, INITIAL_CAPACITY + 1);

    status = int_vec_resize(NULL, 1);
    assert_false(status);

    for (int i = 0; i < INITIAL_CAPACITY + 1; i++) {
        status = int_vec_push_back(&vec, i);
        assert_true(status);
    }

    assert_int_equal(vec.size, INITIAL_CAPACITY + 1);
    assert_int_equal(vec.capacity, INITIAL_CAPACITY + 1);

    status = int_vec_resize(&vec, INITIAL_CAPACITY);
    assert_true(status);
    assert_int_equal(vec.size, INITIAL_CAPACITY);
    assert_int_equal(vec.capacity, INITIAL_CAPACITY);

    status = int_vec_resize(&vec, 0);
    assert_true(status);
    assert_int_equal(vec.size, 0);
    assert_int_equal(vec.capacity, 0);
    assert_null(vec.buffer);

    int_vec_clear(&vec);
}

/**
 * @brief This function tests the shrink_to_fit function of int_vec_m.
 *
 * It first reserves space for UINT8_MAX elements in the vector.
 * Then it pushes UINT8_MAX/2 elements into the vector.
 * It then checks if the capacity and size of the vector are as expected.
 * Finally, it calls the shrink_to_fit function and checks if the capacity and size are adjusted accordingly.
 *
 * @param state A pointer to the test state.
 */
static void shrink_to_fit_tests(void **state) {
    (void) state;
    bool status;
    int_vec_m vec = {0};
    status        = int_vec_reserve(&vec, UINT8_MAX);
    assert_true(status);

    for (int i = 0; i < (UINT8_MAX / 2); i++) {
        status = int_vec_push_back(&vec, i);
        assert_true(status);
    }

    assert_int_equal(vec.capacity, UINT8_MAX);
    assert_int_equal(vec.size, UINT8_MAX / 2);

    status = int_vec_shrink_to_fit(&vec);
    assert_true(status);
    assert_int_equal(vec.capacity, UINT8_MAX / 2);
    assert_int_equal(vec.size, UINT8_MAX / 2);

    int_vec_clear(&vec);
}

static void iter_tests(void **state) {
    (void) state;
    bool status;
    int_vec_m vec;
    status = int_vec_init(&vec);

    assert_true(status);
    for (int i = 0; i < UINT8_MAX; i++) {
        status = int_vec_push_back(&vec, i);
        assert_true(status);
    }

    int value;
    int compare = 0;
    vec_iter_start(&vec, value) {
        assert_int_equal(compare, value);
        compare++;
    }
    vec_iter_end;

    int_vec_clear(&vec);
}

/**
 * @file main.c
 * @brief Main file for running the unit tests.
 */
int main(void) {
    const struct CMUnitTest tests[] = {cmocka_unit_test(init_vector_tests),
                                       cmocka_unit_test(create_vector_tests),
                                       cmocka_unit_test(reserve_vector_tests),
                                       cmocka_unit_test(push_back_tests),
                                       cmocka_unit_test(pop_back_tests),
                                       cmocka_unit_test(at_tests),
                                       cmocka_unit_test(back_tests),
                                       cmocka_unit_test(front_tests),
                                       cmocka_unit_test(data_tests),
                                       cmocka_unit_test(capacity_tests),
                                       cmocka_unit_test(size_tests),
                                       cmocka_unit_test(empty_tests),
                                       cmocka_unit_test(insert_tests),
                                       cmocka_unit_test(resize_tests),
                                       cmocka_unit_test(shrink_to_fit_tests),
                                       cmocka_unit_test(iter_tests)};

    return cmocka_run_group_tests(tests, NULL, NULL);
}