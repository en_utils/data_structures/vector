//
// Created by everythingnice on 3/8/24.
//

#ifndef DATA_STRUCTURES_M_VEC_ITER_H
#define DATA_STRUCTURES_M_VEC_ITER_H

#define vec_iter_start(vec, out)                                                                                       \
    for (int iter_i = 0; iter_i < (vec)->size; iter_i++) {                                                             \
        out = (vec)->buffer[iter_i];
#define vec_iter_end }

#endif    // DATA_STRUCTURES_M_VEC_ITER_H
