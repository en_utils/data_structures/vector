//
// Created by everythingnice on 3/7/24.
//

#ifndef DATA_STRUCTURES_M_VEC_H
#define DATA_STRUCTURES_M_VEC_H

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define INITIAL_CAPACITY 4
#define GROWTH_MODIFIER 2
#define OPERATION_ERROR UINT64_MAX

#define VECTOR_CREATE_HEADER(label, prefix, type)                                                                      \
    typedef struct label {                                                                                             \
        type *buffer;                                                                                                  \
        size_t size;                                                                                                   \
        size_t capacity;                                                                                               \
    } label;                                                                                                           \
                                                                                                                       \
    label *prefix##_create(void);                                                                                      \
    bool prefix##_init(label *vec);                                                                                    \
    bool prefix##_reserve(label *vec, size_t capacity);                                                                \
    static inline void prefix##_clear(label *vec) {                                                                    \
        free(vec->buffer);                                                                                             \
        vec->buffer   = NULL;                                                                                          \
        vec->size     = 0;                                                                                             \
        vec->capacity = 0;                                                                                             \
    }                                                                                                                  \
                                                                                                                       \
    static inline void prefix##_destory(label *vec) {                                                                  \
        if (NULL != vec) {                                                                                             \
            if (NULL != vec->buffer) {                                                                                 \
                prefix##_clear(vec);                                                                                   \
            }                                                                                                          \
            free(vec);                                                                                                 \
            vec = NULL;                                                                                                \
        }                                                                                                              \
    }                                                                                                                  \
                                                                                                                       \
    static inline size_t prefix##_capacity(label *vec) {                                                               \
        if (NULL == vec) {                                                                                             \
            return OPERATION_ERROR;                                                                                    \
        }                                                                                                              \
        return vec->capacity;                                                                                          \
    }                                                                                                                  \
                                                                                                                       \
    static inline size_t prefix##_size(label *vec) {                                                                   \
        if (NULL == vec) {                                                                                             \
            return OPERATION_ERROR;                                                                                    \
        }                                                                                                              \
        return vec->size;                                                                                              \
    }                                                                                                                  \
                                                                                                                       \
    static inline type prefix##_pop_back(label *vec) {                                                                 \
        assert(vec->size);                                                                                             \
        return vec->buffer[--vec->size];                                                                               \
    }                                                                                                                  \
                                                                                                                       \
    static inline type prefix##_at(label *vec, size_t index) {                                                         \
        assert(vec);                                                                                                   \
        assert(vec->buffer);                                                                                           \
        assert(vec->size >= index);                                                                                    \
        return vec->buffer[index];                                                                                     \
    }                                                                                                                  \
                                                                                                                       \
    static inline type prefix##_back(label *vec) {                                                                     \
        assert(vec);                                                                                                   \
        assert(vec->buffer);                                                                                           \
        assert(1 <= vec->size);                                                                                        \
        return vec->buffer[vec->size - 1];                                                                             \
    }                                                                                                                  \
                                                                                                                       \
    static inline type *prefix##_data(label *vec) {                                                                    \
        if (NULL == vec) {                                                                                             \
            return NULL;                                                                                               \
        }                                                                                                              \
        return vec->buffer;                                                                                            \
    }                                                                                                                  \
                                                                                                                       \
    static inline bool prefix##_empty(label *vec) {                                                                    \
        assert(vec);                                                                                                   \
        if (0 == vec->size) {                                                                                          \
            return true;                                                                                               \
        }                                                                                                              \
        return false;                                                                                                  \
    }                                                                                                                  \
                                                                                                                       \
    static inline type *prefix##_front(label *vec) {                                                                   \
        assert(vec);                                                                                                   \
        return vec->buffer;                                                                                            \
    }

#define VECTOR_CREATE_SOURCE(label, prefix, type)                                                                      \
    label *prefix##_create(void) {                                                                                     \
        label *vec = malloc(sizeof(label));                                                                            \
        if (!vec) {                                                                                                    \
            return vec;                                                                                                \
        }                                                                                                              \
        if (!prefix##_init(vec)) {                                                                                     \
            free(vec);                                                                                                 \
            return NULL;                                                                                               \
        }                                                                                                              \
        return vec;                                                                                                    \
    }                                                                                                                  \
                                                                                                                       \
    bool prefix##_init(label *vec) {                                                                                   \
        if (NULL == vec) {                                                                                             \
            return false;                                                                                              \
        }                                                                                                              \
        vec->capacity = INITIAL_CAPACITY;                                                                              \
        vec->size     = 0;                                                                                             \
        return (vec->buffer = malloc(INITIAL_CAPACITY * sizeof(type))) != NULL;                                        \
    }                                                                                                                  \
                                                                                                                       \
    bool prefix##_reserve(label *vec, size_t capacity) {                                                               \
        void *tmp = NULL;                                                                                              \
        if (NULL == vec || vec->capacity >= capacity) {                                                                \
            return false;                                                                                              \
        }                                                                                                              \
                                                                                                                       \
        if (NULL == vec->buffer) {                                                                                     \
            tmp = (type *) calloc(capacity, sizeof(type));                                                             \
        }                                                                                                              \
        else {                                                                                                         \
            tmp = realloc(vec->buffer, capacity * sizeof(type));                                                       \
            if (NULL == tmp) {                                                                                         \
                return false;                                                                                          \
            }                                                                                                          \
        }                                                                                                              \
                                                                                                                       \
        vec->buffer   = (type *) tmp;                                                                                  \
        vec->capacity = capacity;                                                                                      \
        return true;                                                                                                   \
    }                                                                                                                  \
                                                                                                                       \
    bool prefix##_push_back(label *vec, type value) {                                                                  \
        void *tmp = NULL;                                                                                              \
        if (NULL == vec) {                                                                                             \
            return false;                                                                                              \
        }                                                                                                              \
                                                                                                                       \
        if (vec->size == vec->capacity) {                                                                              \
            tmp = realloc(vec->buffer, vec->capacity * GROWTH_MODIFIER * sizeof(value));                               \
            if (NULL == tmp) {                                                                                         \
                return false;                                                                                          \
            }                                                                                                          \
            vec->capacity *= GROWTH_MODIFIER;                                                                          \
            vec->buffer = (type *) tmp;                                                                                \
        }                                                                                                              \
                                                                                                                       \
        vec->buffer[vec->size++] = value;                                                                              \
        return true;                                                                                                   \
    }                                                                                                                  \
                                                                                                                       \
    bool prefix##_insert(label *vec, size_t index, type value) {                                                       \
        void *tmp;                                                                                                     \
        if (index > vec->size) {                                                                                       \
            return false;                                                                                              \
        }                                                                                                              \
                                                                                                                       \
        if (index == vec->size) {                                                                                      \
            return prefix##_push_back(vec, value);                                                                     \
        }                                                                                                              \
                                                                                                                       \
        if (vec->size == vec->capacity) {                                                                              \
            tmp = realloc(vec->buffer, vec->capacity * GROWTH_MODIFIER * sizeof(value));                               \
            if (NULL == tmp) {                                                                                         \
                return false;                                                                                          \
            }                                                                                                          \
            vec->buffer = (type *) tmp;                                                                                \
            vec->capacity *= GROWTH_MODIFIER;                                                                          \
        }                                                                                                              \
                                                                                                                       \
        memmove(vec->buffer + index + 1, vec->buffer + index, (vec->size++ - index) * sizeof(type));                   \
        vec->buffer[index] = value;                                                                                    \
        return true;                                                                                                   \
    }                                                                                                                  \
                                                                                                                       \
    bool prefix##_resize(label *vec, size_t size) {                                                                    \
        void *tmp;                                                                                                     \
        if (NULL == vec) {                                                                                             \
            return false;                                                                                              \
        }                                                                                                              \
                                                                                                                       \
        if (size == vec->capacity) {                                                                                   \
            return true;                                                                                               \
        }                                                                                                              \
                                                                                                                       \
        if (0 == size) {                                                                                               \
            prefix##_clear(vec);                                                                                       \
            return true;                                                                                               \
        }                                                                                                              \
                                                                                                                       \
        tmp = realloc(vec->buffer, size * sizeof(type));                                                               \
        if (NULL == tmp) {                                                                                             \
            return NULL;                                                                                               \
        }                                                                                                              \
                                                                                                                       \
        if (size < vec->size) {                                                                                        \
            vec->size = size;                                                                                          \
        }                                                                                                              \
                                                                                                                       \
        vec->buffer   = tmp;                                                                                           \
        vec->capacity = size;                                                                                          \
        return true;                                                                                                   \
    }                                                                                                                  \
                                                                                                                       \
    bool prefix##_shrink_to_fit(label *vec) {                                                                          \
        void *tmp;                                                                                                     \
        if (NULL == vec) {                                                                                             \
            return false;                                                                                              \
        }                                                                                                              \
                                                                                                                       \
        if (vec->size == vec->capacity) {                                                                              \
            return true;                                                                                               \
        }                                                                                                              \
                                                                                                                       \
        tmp = realloc(vec->buffer, (vec->size == 0 ? 1 : vec->size) * sizeof(type));                                   \
        if (NULL == tmp) {                                                                                             \
            return false;                                                                                              \
        }                                                                                                              \
                                                                                                                       \
        vec->buffer   = (type *) tmp;                                                                                  \
        vec->capacity = vec->size == 0 ? 1 : vec->size;                                                                \
        return true;                                                                                                   \
    }



#endif    // DATA_STRUCTURES_M_VEC_H
